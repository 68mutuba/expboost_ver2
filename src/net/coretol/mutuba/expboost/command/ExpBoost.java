package net.coretol.mutuba.expboost.command;

import net.coretol.mutuba.expboost.Main;
import net.coretol.mutuba.expboost.original_event.ExpBoostFinishEvent;
import net.coretol.mutuba.expboost.original_event.ExpBoostStartEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;
import java.util.List;

public class ExpBoost implements Listener, TabCompleter, CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player){

            Player player = (Player) sender;

            if (args[0].equalsIgnoreCase("")){

            }

            if (args[0].equalsIgnoreCase("boss")){

                if (args[1].equalsIgnoreCase("start")) {
                    Bukkit.getPluginManager().callEvent(new ExpBoostStartEvent());
                }

                if (args[1].equalsIgnoreCase("stop")){
                    Bukkit.getPluginManager().callEvent(new ExpBoostFinishEvent());
                }


            }

        }

        return true;
    }
    public List<String> onTabComplete (CommandSender sender, Command command, String alias, String[]args){
        if (args.length == 1) {
            return Arrays.asList("");
        } else if (args.length == 2) {
            if (args[0].equalsIgnoreCase("")) {
                return Arrays.asList("");
            }
        }
        return null;
    }
}