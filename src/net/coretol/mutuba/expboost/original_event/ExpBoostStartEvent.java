package net.coretol.mutuba.expboost.original_event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ExpBoostStartEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    public ExpBoostStartEvent(){
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

}
