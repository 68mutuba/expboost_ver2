package net.coretol.mutuba.expboost.original_event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ExpBoostFinishEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    public ExpBoostFinishEvent(){
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }
}
