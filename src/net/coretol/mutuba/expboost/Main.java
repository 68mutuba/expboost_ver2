package net.coretol.mutuba.expboost;

import net.coretol.mutuba.expboost.bossbar.BossBarManager;
import net.coretol.mutuba.expboost.command.ExpBoost;
import net.coretol.mutuba.expboost.event.ExpBoostFinish;
import net.coretol.mutuba.expboost.event.ExpBoostStart;
import net.coretol.mutuba.expboost.event.Join;
import net.coretol.mutuba.expboost.event.Left;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Main extends JavaPlugin implements Listener {

    public static Main instance;
    public static boolean barboo = false;

    HashMap<Player,BossBar> bars = new HashMap<>();

    @Override
    public void onEnable(){

        Main.instance = this;

        getServer().getPluginManager().registerEvents(this,this);
        getServer().getPluginManager().registerEvents(new ExpBoostStart(),this);
        getServer().getPluginManager().registerEvents(new ExpBoostFinish(),this);
        getServer().getPluginManager().registerEvents(new Join(),this);
        getServer().getPluginManager().registerEvents(new Left(),this);
        Objects.requireNonNull(getCommand("expboost")).setExecutor(new ExpBoost());

        BossBarManager.onEnable();
    }

    @Override
    public void onDisable() {



    }



}
