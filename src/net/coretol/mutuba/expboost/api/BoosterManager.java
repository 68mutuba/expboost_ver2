package net.coretol.mutuba.expboost.api;

import com.mysql.fabric.xmlrpc.base.Array;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class BoosterManager {

    //１，ブースターのHashMapでの保持→〇
    //２，ブースターをプレイヤーにわたす→〇
    //３，プレイヤーが使用しているブースターの管理
    //４，ブースターの読み込み・保存

    private static HashMap<UUID, ArrayList<Booster>> boosterMap = new HashMap<>();

    public static void giveBooster(UUID uuid, int time, double boostLevel, BoostType boostType){

        Booster booster = new Booster("",time,boostLevel,boostType);

        if (!(boosterMap.containsKey(uuid))){
            ArrayList<Booster> boosterList = new ArrayList<>();
            boosterList.add(booster);
            boosterMap.put(uuid,boosterList);
        } else {
            ArrayList<Booster> boosterList = boosterMap.get(uuid);
            boosterList.add(booster);
            boosterMap.replace(uuid,boosterList);
        }

    }

}
