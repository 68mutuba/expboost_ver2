package net.coretol.mutuba.expboost.api;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Booster {

    //private final int id;//無くてもいいけど管理が楽になる可能性があるから念の為ランダム番号でも付けておく
    private final String name;//ブースターの名前があったほうがGUIに表示するときに楽
    private final int tick;//そのブースターの効果時間
    private final int currentTimeTick;//デフォ値はticksと同じだけど、毎チック１ずつ減らしていって0になったら効果終了(これは減らす)
    private final double boost;//ブーストの効果量
    private final BoostType boostType;

    public Booster(String name,int time , double boostLevel,BoostType boostType){
        this.name = name;
        this.tick = time * 20;
        this.currentTimeTick = tick;
        this.boost = boostLevel;
        this.boostType = boostType;
    }

    public String getName() {
        return name;
    }

    public int getTick() {
        return tick;
    }

    public int getCurrentTimeTick() {
        return currentTimeTick;
    }

    public double getBoost() {
        return boost;
    }

    public BoostType getBoostType(){
        return  boostType;
    }
}