package net.coretol.mutuba.expboost.event;

import net.coretol.mutuba.expboost.Main;
import net.coretol.mutuba.expboost.bossbar.BossBarManager;
import net.coretol.mutuba.expboost.original_event.ExpBoostFinishEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class ExpBoostFinish implements Listener {

    @EventHandler
    public void finish(ExpBoostFinishEvent event){
        Main.barboo = false;

        for (Player player : Bukkit.getOnlinePlayers()) {
            BossBarManager.hide(player);
        }
    }

}
