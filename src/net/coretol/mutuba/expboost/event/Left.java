package net.coretol.mutuba.expboost.event;

import net.coretol.mutuba.expboost.Main;
import net.coretol.mutuba.expboost.bossbar.BossBarManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class Left implements Listener {

    @EventHandler
    public void left(PlayerQuitEvent event){
        Player player = event.getPlayer();
        if (Main.barboo){
            BossBarManager.hide(player);
        }
    }
}
