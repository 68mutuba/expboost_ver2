package net.coretol.mutuba.expboost.event;

import com.google.gson.internal.$Gson$Preconditions;
import net.coretol.mutuba.expboost.Main;
import net.coretol.mutuba.expboost.bossbar.BossBarManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class Join implements Listener {

    @EventHandler
    public void join(PlayerJoinEvent event){
        Player player = event.getPlayer();
        if (Main.barboo){
            BossBarManager.show(player);
        }
    }
}
