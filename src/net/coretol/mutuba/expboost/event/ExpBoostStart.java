package net.coretol.mutuba.expboost.event;

import net.coretol.mutuba.expboost.Main;
import net.coretol.mutuba.expboost.bossbar.BossBarManager;
import net.coretol.mutuba.expboost.bossbar.Progress;
import net.coretol.mutuba.expboost.original_event.ExpBoostStartEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class ExpBoostStart implements Listener {

    @EventHandler
    public void start(ExpBoostStartEvent event) {
        Main.barboo = true;
        BossBarManager.onEnable();
        Progress.progress();
    }

}
