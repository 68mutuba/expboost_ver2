package net.coretol.mutuba.expboost.bossbar;

import net.coretol.mutuba.expboost.Main;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class BossBarManager {

    private static final HashMap<Player, BossBar> bars = new HashMap<>();

    // ここも変えてね！
    private static final String bossBarTitle = "経験値ブースト中";
    private static final BarColor barColor = BarColor.GREEN;
    private static final BarStyle barStyle = BarStyle.SOLID;
    private static final BarFlag barFlags = BarFlag.PLAY_BOSS_MUSIC;


    public static void onEnable() {
        bars.clear();
        if (Main.barboo) {
            for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
                BossBar bar = Bukkit.createBossBar(bossBarTitle, barColor, barStyle, barFlags);
                bar.addPlayer(onlinePlayer);
                bars.put(onlinePlayer, bar);
            }
        }
    }

    public static void onJoinPlayer(Player player) {
        if (bars.containsKey(player)) {
            BossBar bar = bars.get(player);
            bar.addPlayer(player);
        } else {
            BossBar bar = Bukkit.createBossBar(bossBarTitle, barColor, barStyle, barFlags);
            bar.addPlayer(player);
            bars.put(player, bar);
        }
    }

    public static void hide(Player player) {
        if (bars.containsKey(player)) {
            BossBar bar = bars.get(player);
            bar.removeAll();
        }
    }

    public static void show(Player player) {
        if (bars.containsKey(player)) {
            BossBar bar = bars.get(player);
            bar.addPlayer(player);
        }
    }

    /**
     * ボスバーの進行具合を変更する
     * @param player 対象のプレイヤー
     * @param value  設定する値(0.0~1.0の間で指定)
     * @throws IllegalArgumentException valueが0.0~1.0外で指定されたときに出します。
     */
    public static void changeProgress(Player player,double value) {
        if (value < 0 || value > 1.0) throw new IllegalArgumentException(String.format("%f is illegal progress value",value));
        else {
            if (bars.containsKey(player)) {
                BossBar bar = bars.get(player);
                bar.setProgress(value);
            }
        }
    }

    public static double getProgress(Player player) {
        if (bars.containsKey(player)) {
            return bars.get(player).getProgress();
        } else {
            return 0.0;
        }
    }

    public static void changeTitle(Player player,String newTitle) {
        if (bars.containsKey(player)) {
            bars.get(player).setTitle(newTitle);
        }
    }
}
